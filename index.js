const { app, BrowserWindow } = require('electron')

function createWindow () {
  const win = new BrowserWindow({
    title: "Flausch for your RAM - the best electron app ever!",
    contextIsoltaion: true
  })

  win.loadURL('https://flauschehorn.de')
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow. etAllWindows().length === 0) {
    createWindow()
  }
})
